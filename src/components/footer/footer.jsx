import React from 'react';

import './_footer.scss';

const FooterComponent = () => (
    <footer className="footer">
        Check out my
        { ' ' }
        <a href="https://chriscampbell.codes">portfolio site</a>
        { ' ' }
        or my
        { ' ' }
        <a href="https://gitlab.com/chriscampbell">gitlab</a>
        { ' ' }
        for other projects.
    </footer>
);

export default FooterComponent;
