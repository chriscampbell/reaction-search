import React from 'react';

import './_header.scss';

const HeaderComponent = () => (
    <header className="header">
        <h1 className="header__title">Reaction Search</h1>
    </header>
);

export default HeaderComponent;
