import React from 'react';
import PropTypes from 'prop-types';
import ResultComponent from 'components/result/result';
import { ReactComponent as SpinnerIcon } from './spinner.svg';

import './_listing.scss';


const ListingComponent = ({ fetchResults, isFetchingResults, results }) => {
    let resultsMarkup;
    let loadMoreText;

    // Generate the list of results or show a 'No results' message
    if (results && results.length) {
        resultsMarkup = results.map((result) => <ResultComponent key={result.id} data={result} />);
    } else {
        resultsMarkup = 'No results.';
    }

    // Generate the markup shown in the 'Load more' button
    if (isFetchingResults) {
        loadMoreText = (
            <span>
                <SpinnerIcon />
                Loading...
            </span>
        );
    } else {
        loadMoreText = (
            <span>Load more</span>
        );
    }

    return (
        <div className="listing">
            { resultsMarkup }

            <div className="listing__load-more-wrapper">
                <button
                    type="button"
                    onClick={() => fetchResults()}
                    disabled={isFetchingResults}
                >
                    {loadMoreText}
                </button>
            </div>
        </div>
    );
};

ListingComponent.propTypes = {
    fetchResults: PropTypes.func.isRequired,
    isFetchingResults: PropTypes.bool.isRequired,
    results: PropTypes.arrayOf(PropTypes.shape()).isRequired,
};

export default ListingComponent;
