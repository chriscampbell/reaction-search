import React, { useState } from 'react';
import Axios from 'axios';

import './_app.scss';

import HeaderComponent from 'components/header/header';
import FilterComponent from 'components/filter/filter';
import ListingComponent from 'components/listing/listing';
import FooterComponent from 'components/footer/footer';

const App = () => {
    const [results, setResults] = useState([]);
    const [paginationOffset, setPaginationOffset] = useState(0);
    const [lastQuery, setLastQuery] = useState(null);
    const [isFetchingResults, setIsFetchingResults] = useState(false);
    const [error, setError] = useState(null);

    const fetchResults = (newQuery = null) => {
        let query = newQuery;

        // If the newQuery is null use the lastQuery, otherwise update lastQuery
        // to the value of newQuery.
        // This means we can call fetchResults without a query string such as
        // when the user clicks 'Load more'.
        if (newQuery == null) {
            query = lastQuery;
        } else {
            setLastQuery(query);
        }

        setIsFetchingResults(true);

        Axios.get('//api.giphy.com/v1/gifs/search', {
            params: {
                q: query,
                offset: paginationOffset,
                api_key: process.env.REACT_APP_GIPHY_API_KEY,
            },
        })
            .then((response) => {
                // If query matches lastQuery, append the new results to the
                // existing results. Otherwise replace any existing results with
                // the new results.
                if (query === lastQuery) {
                    let mergedResults = results;
                    let filteredNewResults = response.data.data;

                    // Filter out any new results that we are already have
                    filteredNewResults = response.data.data.filter((newResult) => {
                        const resultAlreadyExists = results.some((result) => result.id === newResult.id);

                        return !resultAlreadyExists;
                    });

                    mergedResults = mergedResults.concat(filteredNewResults);

                    setResults(mergedResults);
                } else {
                    setResults(response.data.data);
                }

                setPaginationOffset(response.data.pagination.count + response.data.pagination.offset);
            })
            .catch((response) => {
                setError(response);
            })
            .finally(() => {
                setIsFetchingResults(false);
            });
    };

    return (
        <div className="App">
            <div className="container">
                <HeaderComponent />
                <FilterComponent fetchResults={fetchResults} />
                <ListingComponent
                    fetchResults={fetchResults}
                    isFetchingResults={isFetchingResults}
                    results={results}
                />
                <FooterComponent />
                {error}
            </div>
        </div>
    );
};

export default App;
