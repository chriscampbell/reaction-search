import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as SearchIcon } from './search.svg';

import './_filter.scss';

const filterComponent = ({ fetchResults }) => {
    const [query, setQuery] = useState('');

    const defaultSearchQueries = [
        'Shocked',
        'Doggo',
        'Confused monkey',
        'Coffee',
    ];

    // Do an initial search on page load using a random query to show some gifs
    useEffect(() => {
        const randomQuery = defaultSearchQueries[Math.floor(Math.random() * defaultSearchQueries.length)];

        setQuery(randomQuery);

        fetchResults(randomQuery);
    }, []);

    // filterSubmit - stop the form from submitting and then call 'fetchResults'
    const filterSubmit = (e) => {
        e.preventDefault();

        fetchResults(query);
    };

    return (
        <div className="filter">
            <form method="POST" className="filter__form" onSubmit={filterSubmit}>
                <input
                    type="text"
                    name="filter-query"
                    className="filter__query"
                    value={query}
                    onChange={(e) => setQuery(e.target.value)}
                />
                <button
                    type="submit"
                    name="filter-submit"
                    className="filter__submit  button"
                >
                    <SearchIcon title="Search" />
                </button>
            </form>
        </div>
    );
};

filterComponent.propTypes = {
    fetchResults: PropTypes.func.isRequired,
};

export default filterComponent;
