import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as CloseIcon } from './close.svg';

import './_result.scss';

const ResultComponent = ({ data }) => {
    const [selected, setSelected] = useState(false);

    let titleMarkup;
    let sourceLinkMarkup;

    // Create the 'title' markup
    if (data.title) {
        let title = data.title.toLowerCase();

        // Trim 'GIF'
        title = title.replace('gif', ' ');

        // Trim 'By X' from the title
        [title] = title.split(' by ');

        // Capitalize the first letter
        title = title.charAt(0).toUpperCase() + title.slice(1);

        titleMarkup = (
            <div className="result__title">
                <a
                    href={data.url}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    {title}
                </a>
            </div>
        );
    } else {
        titleMarkup = '';
    }

    // Create the 'source' link markup
    if (data.source) {
        sourceLinkMarkup = (
            <a
                href={data.source}
                target="_blank"
                rel="noopener noreferrer"
            >
                {data.source_tld.replace('www.', '')}
            </a>
        );
    } else {
        sourceLinkMarkup = 'Unknown source.';
    }

    return (
        <div className={`result  ${selected ? 'result--selected' : ''}`}>
            {/* Image */}
            <button
                type="button"
                className="result__image-button"
                onClick={() => setSelected(!selected)}
            >
                <img
                    className="result__image"
                    src={data.images.fixed_height.url}
                    alt={data.title}
                />
            </button>

            {/* Details - START */}
            <div className="result__details">
                {/* Title */}
                {titleMarkup}

                {/* Close button */}
                <button
                    type="button"
                    className="result__details-close"
                    onClick={() => setSelected(false)}
                >
                    <CloseIcon />
                </button>

                {/* Direct URL */}
                <div className="result__details-item">
                    <div>
                        <label htmlFor={`${data.url}-url`}>
                            Direct URL:
                        </label>
                    </div>
                    <div>
                        <input
                            type="text"
                            id={`${data.url}-url`}
                            value={data.images.fixed_height.url}
                            readOnly="readonly"
                            onClick={(e) => { e.target.select(); }}
                        />
                    </div>
                </div>

                {/* Source URL */}
                <div className="result__details-item">
                    <div>
                        Source:
                    </div>
                    <div>
                        {sourceLinkMarkup}
                    </div>
                </div>
            </div>
            {/* Details - END */}
        </div>
    );
};

ResultComponent.propTypes = {
    data: PropTypes.shape().isRequired,
};

export default ResultComponent;
